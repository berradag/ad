import os
import re
import sys
import ast
import json
import collections
import functools
import numpy
import itertools
import glob
import dask.dataframe as dd
import logging
logging.basicConfig(format='%(asctime)s %(levelname)s [%(filename)s:%(lineno)d] %(message)s', level=logging.DEBUG)

#----------------------------------------------------------
#Function to parse an arff file to a context input file 
#(format explained in README)
#
#Takes the full path of the arff input path and optionally
# the full path of the output file
#
#By default, if no output file is supplied, the output of 
# this function is saved in the file whose path is given by
# replacing the arff extension in the input file by a csv 
#extension
#----------------------------------------------------------
def parseArff2CSV(input_file,output_file='',gt_attribute=['y'],excluded_attributes=[]):
	if not os.path.isfile(input_file): #
		sys.exit(input_file+""" does not exist! 
		         No file to be converted.""")
	else:
		if not input_file.endswith('.arff'):
			sys.exit("The input file should be in .arff format not in "+os.path.splitext(input_file)[1]+" format. No conversion done.")
		else:
			logging.debug('Parsing '+input_file+'...')
			with open(input_file,'r') as f:
				head,tail = f.read().split('@data\n') #split file in two (separator @data\n line).
																 # The part that comes after the separaror (i.e tail is the data) and the part before (i.e head) contains the attributes
			#capturing the attribute names and values
			att_r = re.compile("""@attribute\s+        # find the case-insensitive @attribute anchor and associated whitespaces
                                  (?P<att>             # starting a named group that includes both attribute name and attribute types/values
                                  (?P<att_name>[^\s\{\}]+)\s* # finding the attribute name (i.e list of characters-that are not whitespaces or curly brackets-
                                                             # until the next series of whitespaces)
                                  ((?P<att_value>numeric|string|date\s+(?P<date>.+)|\{\S+\}) # specifying an attribute is either a type (either numeric,
                                                                                             # string or  
                                                                                             # a date followed by a string giving the value of the date)
                                                                                             # or in the case of nominal/categorical attributes a list of possibles attribute 
                                                                                             # values delimited with curly brackets
                                  \n*)+ #a newline may follow the attribute description and there may be more than one attribute description
                                  )"""
                                  ,re.I|re.X) #the ignorecase flag is used since any of the keywords (e.g @attribute, date, numeric can be lowercase or uppercase)
            #constructing the list of attribute names as well as a dictionary containing attribute names and associated values
			attributes = (
                 collections.OrderedDict(
                 (m.group('att_name'),ast.literal_eval(m.group('att_value').translate({ord('{'):'["',ord('}'):'"]',ord(','):'","'})))
			     for m in re.finditer(att_r,head))
			 )
			attributes_list = list(attributes.keys())
			index_excluded = [ attributes_list.index(e) for e in excluded_attributes ]
			index_groundtruth = [ attributes_list.index(e) for e in gt_attribute]
			filtered_attributes = [ a for a in attributes_list if a not in excluded_attributes+gt_attribute] #filter list of attributes to remove ground truth attribute
																												     # and excluded attributes
			colnames = ( 'Objects,'+','.join(k if (set(attributes[k])=={'0','1'} or set(attributes[k])=={'-1','1'}  or set(map(str.lower,attributes[k]))=={'yes','no'}) 
                         else ','.join(k+':'+v for v in attributes[k]) for k in filtered_attributes)+'\n' )
			logging.debug('Attributes captured...')
			if all(set(attributes[k])=={'1','0'} for k in filtered_attributes): #if all the attributes are already binarized, no need for further processed, just copy the data
																				#while removing excluded attributes and ground truth attributes
				data = numpy.array([a.split(',') for a in tail.strip('\n').split('\n')])
				data = numpy.delete(data,numpy.s_[index_excluded+index_groundtruth],axis=1)
				first_col = numpy.array(['Entity'+str(i+1) for i in range(data.shape[0])])
				data = numpy.column_stack((first_col,data))
				data = '\n'.join(','.join(e) for e in data)
			else:
				#getting the data block
				split_data = numpy.array(tail.strip('\n').split('\n')) #split data in lines while removing trailing newlines
            #create the file header. The header is a comma-separated string enumerating the column names where
            # a column name is:
            #-an attribute value prefixed by its corresponding attribute name if the attribute has non binarized
            #- the attribute name if the attribute has binarized values
            # The first column name has a value of 'Objects' and simply refers
            # to the fact that the first column contains the name/identifier of the data points/objects)
				index_data = set(range(len(attributes_list)))-set(index_excluded)-set(index_groundtruth) #indexes of the attributes that are not excluded or part of the ground truth
				data = ''
				rep = {'no':'0','yes':'1','-1':'0','1':'1'} #dictionary used to binarize values yes and no and -1
				for i in range(len(split_data)): #converting each data line into a boolean vector
					line = split_data[i].split(',') # split line at commas
					data_line = 'Entity'+str(i+1)+','
					for j in index_data: # iterate through attributes that are not part of the ground truth or excluded
						if set(attributes[attributes_list[j]])=={'1','0'}:
							data_line += line[j]+','
						elif set(map(str.lower,attributes[attributes_list[j]])) == {'yes','no'} or set(attributes[attributes_list[j]])=={'1','-1'}:		
							data_line += functools.reduce(lambda a, kv: a.replace(*kv), rep.items(), line[j])+',' # if an attribute has value yes/no, replace yes by 1 and no by 0
																											  # or if it has value -1/1 replace by 0/1 
						else:
							l = [0]*len(attributes[attributes_list[j]]) #create a list of 0 with length equal to the number of values of the attribute
							if line[j] in attributes[attributes_list[j]]: # check existence of value in case of missing value
								l[attributes[attributes_list[j]].index(line[j])] = 1 #set to 1 the value of the column corresponding to the value of the attribute in the data line
							data_line += ','.join(map(str,l))+','
					data += data_line.strip(',')+'\n' #fully converted data
			logging.debug('Data converted...')
			if output_file == '': #if no output file is given, change the value of output_file to a default path
				output_file = os.path.splitext(input_file)[0]+'.csv' #the default value is simply the path to the input file with the arff extension replaced by a csv extension
			if os.path.isdir(output_file): # if only a directory is specified as an outputfile, the output file is saved under the specified directory
										   # with the same name as the input file ('arff' extension replaced by '.csv')
				output_file = ( os.path.join(output_file,
				                os.path.splitext(os.path.basename(input_file))[0]
				                + '.csv')
				               )
			logging.debug('Writing output file '+output_file+'...')
			with open(output_file,'w') as f:
				f.write(colnames+data) #write the header and converted data to the output file
			logging.debug('Output file '+output_file+' written...')
			#return(output_file)
				
				
#----------------------------------------------------------
#Function to parse an arff file and construct 
# a ground truth file
# Such a file is a single column CSV that only contains
# the name/identifier of the outlier data points
#----------------------------------------------------------
def createGroundTruth(arff_file,outlier_value,gt_attribute='y',output_file=''): 
	# arff_file and output_file are the input and output file of this function respectively
	# gt_attribute is the name of the attribute whose value determines whether a data point is an outlier or not
	# outlier_value is the value that gt_attribute takes when a data point is an outlier
	if not os.path.isfile(arff_file): #
		sys.exit(arff_file+""" does not exist! 
		         No ground truth file created.""")
	else:
		if not arff_file.endswith('.arff'):
			sys.exit("The input file should be in .arff format not in "+os.path.splitext(arff_file)[1]+" format. No ground truth file created.")
		else:
			logging.debug('Creating the ground truth for '+arff_file+'...')
			with open(arff_file,'r') as f:
				head,data_block = f.read().split('@data\n')#split file in two (separator @data\n line).
																 # The part that comes after the separator (i.e data_block is the data) and the part before (i.e head) contains the attributes
			attnames_r = re.compile("""@attribute\s+        # find the case-insensitive @attribute anchor and associated whitespaces
                         (?P<att_name>[^\s\{\}]+)      # finding the attribute name (i.e list of characters that are not whitespaces and/or curly brackets
                                                       # until the next series of whitespaces)
                         """
                         ,re.I|re.X)
			attributes = [m.group('att_name') for m in re.finditer(attnames_r,head)] #construct list of attribute names
			index_gt = attributes.index(gt_attribute) #position of the ground truth attribute in the attributes list
			logging.debug('Position of ground truth attribute found...')
         # Regular expression to extract the value of gt_attribute for all lines of data (value at position index_gt)
			data_l = re.compile(r"""\n* #data line can start with a newline
                    ([^\s,]+,){"""+str(index_gt)+"""} #escape all comma-separated values until position index_gt
                    ((?P<val>[^\s,]+),*)   #extract the value at position index_gt (named group 'val')
                    (.*\n)""",re.X|re.I|re.M)
			line_count = 1 #counter for the number of data lines (i.e data points)
			outliers=''
			for m in re.finditer(data_l,data_block): #for each data line
				if m.group('val') == outlier_value: #compare the value of the ground truth attribute to outlier_value and if they match
					outliers += 'Entity'+str(line_count)+'\n'  # add name of the entities to the outliers' list 
					                                          # {converted to string to be written to file directly)
				line_count += 1
			logging.debug('Ground truth column and outlier entities retrieved...')
			if output_file == '': #if no output file is given, change the value of output_file to a default path
				output_file = os.path.splitext(arff_file)[0]+'_gt.csv' #the default value is simply the path to the input file with the arff extension replaced by a csv extension
			if os.path.isdir(output_file): # if only a directory is specified as an outputfile, the output file is saved under the specified directory
										   # with the same name as the input file ('arff' extension replaced by '.csv')
				output_file = ( os.path.join(output_file,
				                os.path.splitext(os.path.basename(arff_file))[0]
				                + '_gt.csv')
				               )
			logging.debug('Writing output file '+output_file+'...')
			with open(output_file,'w') as f:
				f.write(outliers.strip('\n'))
			logging.debug('Output file '+output_file+' written...')
			#return(output_file)

            
# Function that reads ground truth files and returns a dask dataframe

def readGroundTruth(gtfile_list,id_colname='gt',type_colname='',type_gt=''): 
	columns=([id_colname] if type_gt=='' else [id_colname,type_colname])
	if len(gtfile_list) == 1:
		gt = dd.read_csv(gtfile_list,header=None,names=columns,low_memory=False)
	else:
		gt = dd.multi.concat([dd.read_csv(f,header=None,names=columns,low_memory=False) for f in gtfile_list]) #case where we read the ground truth from multiple files
																												#concatenating the contents of all the files read into a single Dataframe
	if type_gt != '':
		gt = gt[gt[type_colname] == type_gt][id_colname]
	else:
		gt = gt[id_colname] 
	return gt  
	
def getGtFilesFromSpec(inputfiles,gt_spec,root_key='datasets'):
	with open(gt_spec) as f:
		spec = json.load(f)
	keys = {e:list(map(lambda x: x.group(),filter(lambda x: x!=None,[re.compile(k).search(e) for k in spec[root_key].keys()])))[0] for e in inputfiles}
	match_type = spec['match_type']
	data_keys = set(i for e in spec[root_key].keys() for i in tuple(spec[root_key][e].keys()))
	match_key = list(map(lambda x: x.group(),filter(lambda x: x!=None,[re.compile(k).search(match_type) for k in data_keys])))[0]
	gt_key = list(data_keys - {match_key})[0]
	if match_type == 'directory':
		gt_files = [ast.literal_eval(spec[root_key][keys[e]][gt_key]) for e in inputfiles if spec[root_key][keys[e]][match_key]==os.path.dirname(e)]
		gt_files = [[os.path.join(spec[root_key][keys[inputfiles[i]]][match_key],g) if not os.path.isabs(g) else g for g in gt_files[i]] for i in range(len(inputfiles))]
	elif match_type == 'file':
		gt_files =  [ast.literal_eval(dic[root_key][keys[e]][gt_key]) for e in inputfiles if os.path.samefile(dic[root_key][keys[e]][match_key],e)]
		gt_files = [[os.path.join(os.path.dirname(spec[root_key][keys[inputfiles[i]]][match_key]),g) if not os.path.isabs(g) else g for g in gt_files[i]] for i in range(len(inputfiles))]
	return gt_files
	
#function that takes multiple glob expressions and returns an iterable that matches all expressions	
def multiple_globs(*patterns): 
	return itertools.chain.from_iterable(glob.iglob(pattern) for pattern in patterns)          
			
				

				
					
					
					
            
            
			
		
