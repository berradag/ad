#!/bin/bash

rootdir='./csv'
datasets=($(ls $rootdir|sed -e 's/\.[^.]*$//'))

context_counts(){
rootdir=$1
dataset=$2

filename=$rootdir'/'$dataset'.csv'


if [[ -f $filename ]]
  then
		total_cols=$(head -1 $filename | sed 's/[^,]//g' | wc -c)
		decrement=1
		num_cols="$(($total_cols-$decrement))"
		total_lines=$(wc -l $filename| awk '{print $1}')
		num_lines="$(($total_lines-$decrement))"
		size_file=$(du -h $filename| awk '{print $1}')
		echo $dataset : $num_lines' objects '$num_cols' attributes filesize '$size_file$'\n'
fi
}

export -f context_counts
parallel  --jobs 4 -k context_counts  ::: $rootdir ::: ${datasets[@]} 
