import numpy
import dask.dataframe as dd
import operator
import collections
import parsers
import re
import logging
logging.basicConfig(format='%(asctime)s %(levelname)s [%(filename)s:%(lineno)d] %(message)s', level=logging.DEBUG)


# build successor relation and edges among concepts

def successor(concepts,c1,c2):
	return c1 < c2 and {c for c in concepts if c1 < c and c < c2} == set()

def edges(concepts):
	return {(c1,c2) for c1 in concepts for c2 in concepts if successor(concepts,c1,c2)}
	
	
# helper functions to format concepts

def tupToIndices(tup): # given a tuple t, returns a tuple whose values are the positions in which tuple t has a value of 1
	return tuple(i for i in range(len(tup)) if tup[i] == 1)
	
def indicesToNames(indices_tup,names_list): #given a list of names and a tuple t containing list positions/indices, returns a tuple of names 
	return tuple(names_list[e] for e in indices_tup)
	
	
# helper functions for filtering redundant implication rules

def impRuleSubsumes(rule1,rule2):
	return rule1[0] <= rule2[0] and rule2[1] <= rule1[1]

def impRuleRedundant(rules,rule):
	return any(impRuleSubsumes(exrule,rule) == True for exrule in rules)

def updateImpRules(rules,r):
	return {rule for rule in rules if not impRuleSubsumes(r,rule)} | {r}

def filterSubsumedImp(rules):
	newRules = set()
	for rule in rules.keys():
		if not(impRuleRedundant(newRules,rule)):
			newRules = updateImpRules(newRules,rule)
	return {newRule : rules[newRule] for newRule in newRules}


class ContextProcessing():
	def __init__(self,input_file,ground_truth,gt_id_colname='gt',gt_type_colname='',gt_type=''):
		logging.debug('Initializing context with input file '+input_file+' and ground truth file(s) '+str(ground_truth)+'...')
		self.inputfile = input_file
		gt_files = ([ground_truth] if isinstance(ground_truth,str) else ground_truth)
		self.gt = (parsers.readGroundTruth(gt_files,id_colname=gt_id_colname,type_colname=gt_type_colname,type_gt=gt_type).values.compute().astype(str) if gt_files!=[] else []) 
		self.full_context = dd.read_csv(input_file,header=0,low_memory=False) #contains all the csv content including the header
		self.header = list(self.full_context.head(1)) # csv header as a list
		self.attributes = self.header[1:]
		self.objects = self.full_context.values.compute()[:,0] # names of the data points
		self.num_attributes = len(self.attributes)
		self.num_objects = len(self.objects)
		self.data = self.full_context.values.compute()[:,1:]
		self.fca_stats = {'total':0,'closures':0, 'fail_canon':0, 'fail_fcbo':0, 'fail_support':0}
		self.scores = {}
		self.fca_properties = { 'min_support':0.5, 'min_confidence':0, 'num_rules':'*'}
		self.concepts = () 
		self.rules = {}
		logging.debug('Context initialized...')
		
	#-------------------------------------------------------------------------
	# AVF/AVC-related methods
	#-------------------------------------------------------------------------	
			
	def avf_probaMatrix(self):
		logging.debug('Computing AVF probability matrix for '+self.inputfile+'...')
		proba = numpy.sum(self.data.astype(numpy.float64),axis=0)
		return numpy.vstack((1-proba/self.num_objects,proba/self.num_objects))
	
	def avf_score(self):
		scoreMatrix = self.avf_probaMatrix()
		logging.debug('Computing AVF score for '+self.inputfile+'...')
		data = self.data.astype(int)
		scores = [ numpy.sum(scoreMatrix[data[j][i]][i] for i in range(len(data[j])))/len(data[j]) 
		          for j in range(self.num_objects) ]
		full_scoring = dict((self.objects[i],scores[i]) for i in range(self.num_objects))
		logging.debug('AVF scores computed for '+self.inputfile+'...')
		return full_scoring
		
	def avc_score(self):
		scoreMatrix = self.avf_probaMatrix()
		avc_scoreMatrix = 0-numpy.log2(scoreMatrix)
		logging.debug('Computing AVC score for '+self.inputfile+'...')
		data = self.data.astype(int)
		scores = [ numpy.sum(avc_scoreMatrix[data[j][i]][i] for i in range(len(data[j])))/len(data[j]) 
		          for j in range(self.num_objects) ]
		full_scoring = dict((self.objects[i],scores[i]) for i in range(self.num_objects))
		logging.debug('AVC scores computed for '+self.inputfile+'...')
		return full_scoring
		          
		
	
	#--------------------------------------------------------------------------
	# FCA-related methods
	#--------------------------------------------------------------------------
	def setFcaProperties(self,dict_prop):
		for k in dict_prop.keys():
			if k in self.fca_properties and dict_prop[k] != self.fca_properties[k]:
				self.fca_properties[k] = dict_prop[k]
		
	#--------------------------------------------------------------------------
	# computeClosure, generateFrom and generateConcepts are a port
	# from the original FCbO algorithm C code
	#--------------------------------------------------------------------------
	
	#-----------------------------------------------------------------------
	# computeClosure: computes closures. Takes into account 
	# minimal support conditions
	#-----------------------------------------------------------------------
	def computeClosure(self,extent,intent,new_attribute):
		table=self.data
		rows = numpy.flatnonzero(table[:,new_attribute]==1)
		C=numpy.zeros((self.num_objects,),dtype=int)
		D=numpy.ones((self.num_attributes,),dtype=int)
		intersect_extent_rows=rows[extent[rows]==1]
		supp = len(intersect_extent_rows)/self.num_objects
		for e in intersect_extent_rows:
			C[e] = 1
			for j in range(self.num_attributes):
				if table[e,j] == 0:
					D[j]=0
		self.fca_stats['closures']+=1
		return C,D,supp
	
	#-------------------------------------------------------------------------------
	#generateFrom: main function of FCbO. 
	#
	#Generates the concepts whose support is above a certain threshold. 
	#
	#Currently a direct port of the original recursive algorithm.
	#For scalability reasons, it might be good to make this function iterative
	#in future versions of the code. 
	#-------------------------------------------------------------------------------	
	def generateFrom(self,extent,intent,new_attribute):
		concepts = {(tuple(extent),tuple(intent)),}
		if all(intent) != 1 and new_attribute <= self.num_attributes:
			for j in range(new_attribute,self.num_attributes):
				if intent[j] == 0:
					C,D,supp = self.computeClosure(extent,intent,j)
					skip = False
					for k in range(j-1):
						if D[k] != intent[k]:
							skip = True
							self.fca_stats['fail_canon'] += 1
							break
						if supp < self.fca_properties['min_support']:
							skip = True
							self.fca_stats['fail_support'] += 1
							break
					if skip == False:
						concept = self.generateFrom(C,D,j+1)
						concepts.update(concept)
		return concepts
		
	#-------------------------------------------------------------------------------
	# format concepts so that extents and intents contain named objects/attributes
	#-------------------------------------------------------------------------------
	
	def formatConcepts(self,concepts):
		transformed_concepts = {(tupToIndices(tup[0]),tupToIndices(tup[1])) for tup in concepts}
		transformed_concepts = {(indicesToNames(tup[0],self.objects),indicesToNames(tup[1],self.attributes)) for tup in transformed_concepts}
		return transformed_concepts
		
	#-------------------------------------------------------------------------------
	# Retrieve attribute names given an object name 
	#-------------------------------------------------------------------------------
	
	def getAttributeNamesFromObjName(self,objname):
		obj_index = numpy.flatnonzero(self.objects==objname)
		if obj_index.size != 0:
			data_line = self.data[obj_index].ravel()
			if 1 in data_line:
				attributes = operator.itemgetter(*numpy.flatnonzero(data_line==1).tolist())(self.attributes)
			else:
				attributes = ()
		else:
			attributes = ()
		return attributes
			
	
	#-------------------------------------------------------------------------------
	# generateConcepts: generates all concepts whose support is above the 
	#'support' parameter
	#-------------------------------------------------------------------------------
	def generateConcepts(self,support): 
		if support != self.fca_properties['min_support']:
			self.setFcaProperties({'min_support':support})
		start_extent = numpy.ones((self.num_objects,),dtype=int) #initialize the extent to an array of ones
		start_intent = numpy.zeros((self.num_attributes,),dtype=int)  #initialize the inttent to an array of zeros
		new_attribute = 0
		logging.debug('Generating FCA (FCbO) concepts for '+self.inputfile+'...')
		concepts = self.generateFrom(start_extent,start_intent,new_attribute) #generate the concepts
		#filter duplicate concepts
		logging.debug('De-duplicating FCA (FCbO) concepts for '+self.inputfile+'...')
		unfiltered_concepts = list(concepts)
		dic = collections.defaultdict(list)
		for i in range(len(unfiltered_concepts)):
			dic[unfiltered_concepts[i][0]].append((i,unfiltered_concepts[i][1].count(1)))
		concept_filter = [max(v,key=operator.itemgetter(1))[0] for v in dic.values()]
		filtered_concepts = set([unfiltered_concepts[c] for c in concept_filter])
		self.fca_stats['total'] = len(filtered_concepts)
		self.concepts = self.formatConcepts(filtered_concepts)
		logging.debug('FCA (FCbO) concepts generation for '+self.inputfile+' completed...')
		
		
	#----------------------------------------------------------------------------
	# Functions to compute the support of an itemset/concept intent (nominal 
	# support or support scaled by the number of objects)
	#----------------------------------------------------------------------------
		
	def getConceptSupport(self,concept_intent):
		support = 0
		for row in self.data:
			indices = [self.attributes.index(e) for e in concept_intent]
			if indices != []:
				vals = row[indices]
				if all(vals == 1):
					support += 1
				else:
					if all(row == 0):
						support += 1
		return support
		
	def getItemsetScaledSupport(self,concept_intent):
		support = self.getConceptSupport(concept_intent)
		return support/self.num_objects
	
	#---------------------------------------------------------------------------------
	#generating non-redundant implication rules from concepts
	#---------------------------------------------------------------------------------
	def generateRules(self,support,confidence):
		logging.debug('Starting rule generation from FCA (FCbO) concepts for '+self.inputfile+'...')
		self.setFcaProperties({'min_support':support,'min_confidence':confidence})
		itemsets = {frozenset(c[1]) for c in self.concepts}
		edg = edges(itemsets)
		supports = {}
		logging.debug('Computation of required itemset supports'+self.inputfile+'...')
		for e in edg:#compute all the supports necessary for the rule computations
			if e[1] not in supports.keys():
				supports[e[1]] = self.getItemsetScaledSupport(e[1])
			if e[0] not in supports.keys():
				supports[e[0]] = self.getItemsetScaledSupport(e[0])
			if e[1]-e[0] not in supports.keys():
				supports[e[1]-e[0]] = self.getItemsetScaledSupport(e[1]-e[0])
		# compute implication rules along with their confidence and lift
		logging.debug('Computation of implication rules'+self.inputfile+'...')
		rules = {(e[0],e[1]-e[0]): (supports[e[1]]/supports[e[0]],supports[e[1]]/(supports[e[0]]*supports[e[1]-e[0]])) 
		                 for e in edg if supports[e[0]] > 0 and supports[e[1]]/supports[e[0]] > self.fca_properties['min_confidence']}
		#filtering redundant rules
		logging.debug('Eliminating redundant rules'+self.inputfile+'...')
		reducedRules = filterSubsumedImp(rules)
		self.rules = reducedRules
		logging.debug('Rule generation for '+self.inputfile+' completed...')
		
	def violations(self,att_list):
		setS = (frozenset({att_list}) if type(att_list) == str else frozenset(att_list))
		ent = 0-sum([numpy.log2(1-self.rules[e][0]) for e in self.rules.keys() if e[0] <= setS and not(e[1] <= setS)])
		entLift = 0-sum([numpy.log2(1-self.rules[e][1]) for e in self.rules.keys() if e[0] <= setS and not(e[1] <= setS)])
		return (ent,entLift)	
	
	def fca_anomaly_score(self):
		logging.debug('Computing FCA-based anomaly scores for '+self.inputfile+'...')
		return {k:self.violations(self.getAttributeNamesFromObjName(k)) for k in self.objects}

	
	#--------------------------------------------------------------------------
	# More general methods
	#--------------------------------------------------------------------------
	
	def score(self,methods,score_file=''): 
	#method is a set of processing methods. 
	#Current valid value: {'avf'},{'fca'},{'avc'},{'avf','fca'},{'avf','avc'},{'avc','fca'} or {'avf','avc','fca'}
	# score_file is the path to the output file. If not provided, defaults to input_file path where
	# input_file extension is replaced by '.scored.csv'
		#ensure that methods doesn't contain values other than 'avf' or 'fca'
		filtered_methods = sorted(filter(lambda x: x=='avf' or x=='fca' or x=='avc',map(str.lower,methods)))
		logging.debug('Computing anomaly scores of context objects for '+self.inputfile+' with method(s) '+' '.join(filtered_methods)+'...')
		score_functions = {'avf':self.avf_score,'avc':self.avc_score,'fca':self.fca_anomaly_score}
		method_headers = {'avf':'AVF score','avc':'AVC score','fca':'FCA confidence score,FCA lift score'} 
		if len(filtered_methods) == 1:
			header = 'Objects,'+method_headers[filtered_methods[0]]+'\n'
			field = method_headers[filtered_methods[0]].replace(' ','_').lower()
			scoreTuple = collections.namedtuple('scoreTuple',field)
			logging.debug('Computing anomaly scores for '+self.inputfile+'...')
			scores = score_functions[filtered_methods[0]]()
			self.scores = {k:(scoreTuple._make([v]) if isinstance(v,float) else scoreTuple._make(v)) for k,v in scores.items()}
			full_scores = sorted(scores.items(),key=operator.itemgetter(1))
			if isinstance(full_scores[0][1],tuple) or isinstance(full_scores[0][1],list):
				file_content = '\n'.join([s[0]+','+str(s[1][0])+','+str(s[1][1]) for s in full_scores])
			else:
				file_content = '\n'.join([s[0]+','+str(s[1]) for s in full_scores])
		else:
			header = 'Objects,'+','.join([method_headers[m] for m in filtered_methods])+'\n'
			fields = sorted(','.join(method_headers.values()).replace(' ','_').lower().split(','))
			logging.debug('Computing anomaly scores for '+self.inputfile+'...')
			scores = {method_headers[m].lower().replace(' ','_'):score_functions[m]() for m in filtered_methods}
			trans_scores = {} # building a dictionary where each type of score is stored using a separate key
			for k,v in scores.items():
				if not ',' in k:
					trans_scores[k]=v
				else:
					new_keys=k.split(',')
					for i in range(len(new_keys)):
						trans_scores[new_keys[i]]={j:l[i] for j,l in scores[k].items()}
			full_scores = {}
			for obj in self.objects:
				obj_score = collections.namedtuple('scoreTuple',sorted(trans_scores.keys()))(*[trans_scores[f][obj] for f in fields])#annotated the scores per object with their type
				full_scores[obj] = obj_score	
			self.scores = full_scores			
			file_content='\n'.join(k+','+','.join(map(str,v)) for k,v in full_scores.items())
		if score_file == '':
			score_file = os.path.splitext(self.inputfile)[0]+'.scored.csv'
		logging.debug('Writing anomaly scores to file '+score_file+'...')
		with(open(score_file,'w')) as f:
			f.write(header+file_content)
			
	def rank(self,ranking_file=''):
		logging.debug('Computing ranking of true positives for '+self.inputfile+'...')
		type_scores={y for x in self.scores.values() for y in x._fields}
		ordering = {'fca':True,'avf':False,'avc':True} # the lower (resp. the higher) the avf score (resp. the fca/avc scores), the more anomalous
		rankings = {}
		if type_scores != set():
			sorted_type_scores = sorted(type_scores)
			for t in type_scores: # get the ranking positions of the true positives (i.e elements in the ground truth)
				order = operator.itemgetter(*filter(lambda x:x in t,ordering.keys()))(ordering)
				ordered_scoring = collections.OrderedDict(sorted({k:getattr(v,t) for k,v in self.scores.items()}.items(),key=operator.itemgetter(1),reverse=order))
				ordered_entities = numpy.array(list(ordered_scoring.keys())).astype(str)
				true_positive_positions = numpy.flatnonzero(numpy.isin(ordered_entities,self.gt))
				ranks = {ordered_entities[i]:i+1 for i in true_positive_positions}
				rankings[t] = ranks
			full_rankings = {}
			for obj in self.gt:
				print('obj',obj)
				if all(obj in rankings[f].keys() for f in sorted_type_scores):
					obj_rank = collections.namedtuple('rankTuple',sorted_type_scores)(*[rankings[f][obj] for f in sorted_type_scores])
					full_rankings[obj] = obj_rank
			self.rankings = full_rankings
			substitutions={'avf|fca|avc':(lambda x: str.upper(x.group())),'confidence|lift':(lambda x: str.capitalize(x.group())),'_':' ','score':'Rank'}
			header = 'Objects,'
			for s in sorted_type_scores:
				new_s = s
				for k in substitutions.keys():
					new_s = re.sub(k,substitutions[k],new_s)
				header += new_s+','
			header = header.strip(',')+'\n'
			file_content='\n'.join(k+','+','.join(map(str,v)) for k,v in full_rankings.items())
			if ranking_file == '':
				ranking_file = os.path.splitext(self.inputfile)[0]+'.ranking.csv'
			logging.debug('Writing rankings to file '+ranking_file+'...')
			with(open(ranking_file,'w')) as f:
				f.write(header+file_content)
		
				

			
		
	
	
