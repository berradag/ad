import parsers
import math
import numpy
import dask.dataframe as dd
import logging
logging.basicConfig(format='%(asctime)s %(levelname)s [%(filename)s:%(lineno)d] %(message)s', level=logging.DEBUG)


#-----------------------------------------------------
# Computes the binary normalized discounted cumulative 
# gain metric (nDCG)
#-----------------------------------------------------
def ndcg(ranks,num_gt):
	logging.debug('Computing the nDCG...')
	dcg = sum([1/math.log2(rank+1) for rank in ranks])
	maxdcg = sum([1/math.log2(i+1) for i in range(1,num_gt+1)])
	return dcg/maxdcg
	
#-----------------------------------------------------
#  Computes the single label ranking loss
#  (adapted from multi-label version in 
#   Tsoumakas G., Katakis I. & Vlahavas I.(2010),
#  'Mining multi-label data'. In Data mining and
#   knowledge discovery handbook, 
#   pp.667-685, Springer US)
#-----------------------------------------------------

def ranking_loss(ranks,num_objects):
	logging.debug('Computing the ranking loss...')
	if isinstance(ranks,list):
		ranks = numpy.array(ranks) 
	complement = numpy.setdiff1d(numpy.arange(1,num_objects+1),ranks)
	#bad_rankings = numpy.array([(i,j) for i in range(len(ranks)) for j in range(len(complement)) if ranks[i] > complement[j]])
	num_bad_rankings = numpy.sum([len(numpy.flatnonzero(r>complement)) for r in ranks])
	return num_bad_rankings/(len(ranks)*len(complement))
	
	
#-------------------------------------------------------------
# Helper functions:
# -to get rankings from file (to dask dataframe)
# -to get the number of true positives (elements in ground truth)
#-------------------------------------------------------------	
	
def getRealRanking(ranking_file,colname=['Rank']):
	ranking = dd.read_csv(ranking_file,header=0,usecols=[c for c in colname],low_memory=False)
	return ranking
	
def getRankings(ranking_file):
	ranking = dd.read_csv(ranking_file,header=0,low_memory=False)
	return ranking
	
def getNumberRelevantEntities(gt_files,idcolname='gt',typecolname='',typegt=''):
	if typegt == '':
		gt=set(parsers.readGroundTruth(gt_files).values.compute())
	else:
		gt=set(parsers.readGroundTruth(gt_files,id_colname=idcolname,type_colname=typecolname,type_gt=typegt).values.compute())
	return len(gt)
