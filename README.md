# AD

## Brief content description

This folder includes 4 types of scripts:
* a Python script (```file_manip_script.py```) to parse ARFF files in context CSV files (more on this format later)
* a Bash script (```dataset_properties.sh```) that computes a few CSV datasets properties (e.g number of objects i.e number of data points, number of attributes (i.e number of columns minus identifier column) and filesize)
* a Python script (```ad_script.py```) to perform two types of anomaly detection on the CSV data (AVF and FCA(Formal Concept Analysis)-based anomaly detection)
* a Python script (```metrics_script.py```) that computes a few metrics (nDCG and ranking loss) on the results of anomaly detection

This folder also contains:
* some publicly available ARFF datasets 
( described in https://sites.google.com/site/gspangsite/sourcecode/categoricaldata and downloadable from https://drive.google.com/open?id=0B_GL5U7rPj1xR0Jld2hKZzF5dzA) in the ```data/``` folder
* CSV files that were produced by parsing the files in the ```data/``` folder with the ```file_manip_script.py``` script in the ```csv/``` folder
* ground truth files corresponding to the files in the ```csv/``` folder (also obtained by parsing the files in the ```data/``` folder with the ```file_manip_script.py``` script) in the ```ground_truth/``` folder
* result files (scores, rankings and metric results with the extensions ```.scored.csv```, ```.ranking.csv```, ```.metrics.csv``` respectively) obtained with the ```ad_script.py``` and ```metrics_script.py``` script in the ```results/``` directory

## Description of the ARFF file parsing

### Description of the parsing output format (context CSV)

The parser script converts input ARFF files (format described in https://www.cs.waikato.ac.nz/ml/weka/arff.html ) to context CSVs.
A context CSV is a CSV file formatted as follows:
```
Objects     , nameAttribute_1          ,nameAttribute_2           ,...,nameAttribute_N
nameObject_1,valueAttribute_1(object_1),valueAttribute_2(object_1),...,valueAttribute_N(object_1)
nameObject_2,valueAttribute_1(object_2),valueAttribute_2(object_2),...,valueAttribute_N(object_2)
.
.
.
nameObject_M,valueAttribute_1(object_M),valueAttribute_2(object_M),...,valueAttribute_N(object_M)
```


where: 
* the ```Objects``` column simply contains the data point identifiers ( ```nameObject_1...nameObject_M``` where ```M``` is the number of data points).
Currently, the identifiers are set to values ```Entity1...EntityM```.

* ```nameAttribute_1...nameAttribute_N``` are the names of the data attributes and are obtained from the names and values of the attributes in the input ARFF file.

In the original ARFF files, the lines describing the attributes are mostly of the form: ```@attribute attribute_name {att_value1,...,att_valueN}```.
A multi-valued attribute in the original ARFF file spawns multiple attributes (and columns) in the context CSV. For example, 
an attribute with attribute name ```a``` with values ```{v_1,..v_n}```) in the original ARFF file will be converted to 
```n``` columns with names of the form ```a:v_i``` (where ```v_i``` is the i-th value of attribute ```a```) in the context CSV.
Binary attributes will retain their name from the original ARFF file in the output CSV context.

*  Except the identifier ```nameObject_j``` (where ```j``` is the j-th object/data point), each data line in the context CSV is a Boolean vector (i.e vector of 0/1) that indicates whether
an object has or doesn't have an attribute.
E.g: The line ```Entity1,0,1,1,0,0,0``` means that the object with identifier ```Entity1``` has attributes 2 and 3 but not attributes 1,4,5 and 6. 

### How to invoke and use the parsing script

The following command can be used to invoke the parsing script (from the same directory where ```file_manip_script.py``` is located):
```
python3 file_manip_script.py [-h] 
                            [--glob_inputpath GLOB_INPUTPATH [GLOB_INPUTPATH ...]]
                            [--outputfile_names OUTPUTFILE_NAMES [OUTPUTFILE_NAMES ...]]
                            [--groundtruth_spec GROUNDTRUTH_SPEC [GROUNDTRUTH_SPEC ...]]

```

Here is a brief explanation of the script options and defaults:
*  ```--glob_inputpath GLOB_INPUTPATH [GLOB_INPUTPATH ...], -g GLOB_INPUTPATH [GLOB_INPUTPATH ...]``` : This option is used to specify the files to be converted. ```GLOB_INPUTPATH``` corresponds to 1 or more space-separated
  glob expressions that match the input files patterns. This option has a default value of ```./data/*.arff```, which means all the ARFF files found in the ```data/``` directory will be
  fetched and converted
* ```--outputfile_names OUTPUTFILE_NAMES [OUTPUTFILE_NAMES ...], -o OUTPUTFILE_NAMES [OUTPUTFILE_NAMES ...]```: This option is used to specify the output file names. If none are specified or
 there are less output file names than input file names, the missing output file names are formed by replacing the '.arff' extension in the input file paths by a '.csv' extension.
* ```--groundtruth_spec GROUNDTRUTH_SPEC [GROUNDTRUTH_SPEC ...], -gt GROUNDTRUTH_SPEC [GROUNDTRUTH_SPEC ...]```: This option points to a JSON specification file that gives the name of
the ground truth attribute (as well as the value this attribute takes when a data point is an outlier) for each input file. The default value for this file is ```./data/data_properties.json```. 
 
Examples:

```python3 file_manip_script.py``` (to run the parsing using all the defaults i.e on all the ARFF files in ```./data```)

```python3 file_manip_script.py -g data/kdd*.arff``` (to run the parsing on the ARFF files in ```./data``` that correspond to the KDDCup99 intrusion detection datasets)

```python3 file_manip_script.py -g data/kdd*.arff data/*census*.arff``` (to run the parsing on the ARFF files in ```./data``` that correspond to the KDDCup99 intrusion detection and Census datasets)

### Helper Bash script description

To obtain information such as the number of objects and attributes in a context CSV and its filesize, you can use the ```dataset_properties.sh``` script.
This script finds these properties for all the ```csv``` files found in the ```./csv/``` folder.
You can invoke this script from the directory in which it is stored with ```./dataset_properties.sh```

## Anomaly detection

### Description of the anomaly detection algorithms, their outputs and their evaluation metrics

We use two different algorithms to perform anomaly detection: AVF and a Formal Concept Analysis(FCA)-based anomaly detection.

For details of the AVF algorithm, we refer to the original *Koufakou, A.; Ortiz, E. G.; Georgiopoulos, M.; Anagnostopoulos, G. C. & Reynolds, K. M. A Scalable and Efficient Outlier Detection Strategy
 for Categorical Data ICTAI 2007, 2007, 210-217* paper.
 We also refer to the abundant FCA literature for details on concept/frequent rule 
 generation algorithms (in particular the paper *Krajca P., Outrata J., Vychodil V.: Parallel Recursive Algorithm for FCA.
In: Belohlavek R., Kuznetsov S. O. (Eds.): Proc. CLA 2008, pp. 71–82*. that describes FCbO, the FCA algorithm whose ported Python implementation we share here, and http://fcalgs.sourceforge.net/ that contains
the original FCbO C code as well Uta Priss' FCA webpage http://www.upriss.org.uk/fca/fca.html that links to many useful resources on FCA).

With AVF, we first compute a probability of occurrence of 0/1 values for each attribute based on the dataset then use these probabilities to compute an anomaly score per object.
In this case, the lower the AVF score the more anomalous the object.
In the case of the FCA-based anomaly detection, we first extract frequent concepts and high confidence association rules from the datasets then score each object
depending on its adherence to the rules. An object that violates many rules and in particular high confidence rules will have a high anomaly score.
These anomaly scores are by default saved in the ```results/``` folder with a ```.scored.csv``` extension.

These anomaly scores are then used to rank the objects from highly anomalous to less anomalous (ascending order of scores for AVF and descending order for FCA).
The ranked lists of objects obtained this way are then matched to the ground truth to determine the positions in the list of the true positives i.e outliers. These positions
are stored by default saved in the ```results/``` folder with a ```.ranking.csv``` extension. The best case scenario is when all true positives are at the top of the rankings, for example,
if there are 12 outliers in the data, they should be ranked from 1 to 12.

We can then use these ranking files to compute a few metrics that measure the quality of a ranking, namely the normalized discounted cumulative gain (nDCG -see *Kalervo Järvelin and Jaana Kekäläinen. 2002. Cumulated gain-based evaluation
of IR techniques. ACM Transactions on Information Systems (TOIS) 20, 4 (2002),
422–446.* for a definition and a detailed rationale) and the ranking loss (single label version
adapted from multi-label version in *Tsoumakas G., Katakis I. & Vlahavas I.(2010),'Mining multi-label data'. In Data mining and knowledge discovery handbook, pp.667-685, Springer US*). The metric 
computations are by default written to ```.metrics.csv``` files in the ```results/``` directory.


### Invoking the anomaly detection script

The anomaly detection script (anomaly score+ranking generation) can be invoked (from the directory in which ```ad_script.py``` is stored) with this command:
```
python3 ad_script.py [-h]
                    [--glob_inputpath GLOB_INPUTPATH [GLOB_INPUTPATH ...]]
                    [--ground_truth_dir GROUND_TRUTH_DIR]
                    [--output_dir OUTPUT_DIR]
                    [--methods [{avf,fca} [{avf,fca} ...]]]
                    [--min_support MIN_SUPPORT]
                    [--min_confidence MIN_CONFIDENCE]
```

Here is a brief description of the command options:
* ```--glob_inputpath GLOB_INPUTPATH [GLOB_INPUTPATH ...], -g GLOB_INPUTPATH [GLOB_INPUTPATH ...]```: works in the same way as the same option in the parsing script to specify which files to process
with anomaly detection algorithms and has a default value of  ```./csv/*.csv```
* ```--ground_truth_dir GROUND_TRUTH_DIR, -gt GROUND_TRUTH_DIR```: is used to specify the directory where the ground truth files are stored. If left unspecified, defaults to ```./ground_truth```
* ```--output_dir OUTPUT_DIR, -o OUTPUT_DIR```: specifies where to output the result files (score and ranking files). Defaults to ```./results``` if left unspecified
* ```--methods [{avf,fca} [{avf,fca} ...]], -m [{avf,fca} [{avf,fca} ...]]```: Specifies which anomaly detection methods to use. Valid choices: ```avf```, ```fca```, ```avf fca``` or ```fca avf```.
If left unspecified, defaults to ```avf```.
* ```--min_support MIN_SUPPORT, -s MIN_SUPPORT```: Is used to specify the first FCA parameter i.e minimum support (that is their minimal frequency of appearance in the data) 
of the concepts to be computed. This value is a float between 0 and 1 and has a default value of 0 (i.e no concept will be discarded from the computation)
* ```--min_confidence MIN_CONFIDENCE, -c MIN_CONFIDENCE```: Is used to specify the second FCA parameter i.e the minimum confidence of the association rules to be computed. This is also a float 
value between 0 and 1 that has a default value of 0 (all rules will be computed)

Example:

```python3 ad_script.py -g './csv/kdd*.csv' './csv/AID*.csv' './csv/cmc*.csv' -m 'avf' 'fca' -s 0.5``` (run anomaly detection on CMC, KDD and AID362 datasets using both FCA and AVF
 with a support value set to 0.5 for FCA)



### Invoking the metrics computation script

The computation of the evaluation metrics can be invoked with this command (from the directory containing ```metrics_script.py```):

```
python3 metrics_script.py [-h]
                         [--glob_inputpath GLOB_INPUTPATH [GLOB_INPUTPATH ...]]
                         [--ground_truth_dir GROUND_TRUTH_DIR]
                         [--output_dir OUTPUT_DIR]
                         [--data_prop DATA_PROP [DATA_PROP ...]]
                         [--first_colname FIRST_COLNAME]
```                         

Its options are as follow:
* ```--glob_inputpath GLOB_INPUTPATH [GLOB_INPUTPATH ...], -g GLOB_INPUTPATH [GLOB_INPUTPATH ...]```: Specifies the input ranking files in the same way as 
the same option in the parsing/anomaly detection scripts. Has a default value of ```./results/*.ranking.csv```
* ```--ground_truth_dir GROUND_TRUTH_DIR, -gt GROUND_TRUTH_DIR``` and   ```--output_dir OUTPUT_DIR, -o OUTPUT_DIR```: same usage as the corresponding options in the anomaly detection script
* ```--data_prop DATA_PROP [DATA_PROP ...], -dp DATA_PROP [DATA_PROP ...]```: points to JSON specification files that matches the input files with their properties (e.g their number of objects).
Has a default value of ```./csv/csv_properties.json```
* ```--first_colname FIRST_COLNAME, -c FIRST_COLNAME```: Specifies the name of the first column in the input files. Default value: ```Objects```

Example: 

```python3 metrics_script.py``` (to compute the metrics for all ranking files in the ```results/``` directory)

