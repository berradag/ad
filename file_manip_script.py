import parsers
import argparse
import glob
from joblib import Parallel, delayed
import os
import json
import sys

parser = argparse.ArgumentParser(description='script to convert ARFF files to CSV')
parser.add_argument('--glob_inputpath','-g',help='Glob expression(s) used to fetch the input files. If none is specified, defaults to ./data/*.arff', nargs='+', default='./data/*.arff')
parser.add_argument('--outputfile_names','-o',help='List of output file names',nargs='+',default='')
parser.add_argument('--groundtruth_spec','-gt',help='JSON specification files matching the input files with their ground truth attribute',nargs='+',default='./data/data_properties.json')
	
if __name__ == '__main__':
	#retrieving the arguments from the command line
	args = parser.parse_args()
	glob_path = args.glob_inputpath
	glob_path = ([glob_path] if isinstance(glob_path,str) else glob_path)
	inputfiles = [f for f in parsers.multiple_globs(*glob_path)]
	if len(inputfiles) == 0:
		sys.exit('No files to process. Stopping run')
	else:
		outputs = args.outputfile_names
		dirname = os.path.dirname(inputfiles[0])
		with open(args.groundtruth_spec,'r') as f:
			gt = json.load(f)['datasets']
		if (outputs == ''):
			output_files = [outputs]*len(inputfiles)
		elif len(outputs) == 1 and os.path.isdir(outputs[0]):
			output_files = [outputs[0]]*len(inputfiles)
		else:
			if len(outputs) == len(inputfiles):
				output_files = outputs
			elif len(outputs) > len(inputfiles):
				output_files = outputs[0:len(inputfiles)]
			else:
				output_files = outputs + ['']*len(inputfiles)-len(outputs)
		gt_attributes = [ gt[f]['att'] for f in map(os.path.basename,inputfiles)] #constructing list of ground truth attributes for input files
		gt_outliers = [ gt[f]['val'] for f in map(os.path.basename,inputfiles)] #constructing list of outlier values for input files
		#creating the CSV files
		Parallel(n_jobs=-1, verbose=1, backend="threading")(
	                                                       delayed(parsers.parseArff2CSV)
	                                                       (input_file=inputfiles[i],output_file=output_files[i],gt_attribute=[gt_attributes[i]])
	                                                        for i in range(len(inputfiles)))

		#Creating the ground truth files
		Parallel(n_jobs=-1, verbose=1, backend="threading")(
	                                                       delayed(parsers.createGroundTruth)
	                                                       (arff_file=inputfiles[i],outlier_value=gt_outliers[i],
	                                                        gt_attribute=gt_attributes[i],output_file=output_files[i])
	                                                        for i in range(len(inputfiles)))
                                                    

	
	
	
