import parsers
import argparse
import metrics
from joblib import Parallel, delayed
import os
import sys
import json
import numpy
import itertools
import re


parser = argparse.ArgumentParser(description='Metrics computation script')
parser.add_argument('--glob_inputpath','-g',help='Glob expression(s) used to fetch the input (ranking) files. If none is specified, defaults to ./results/*.ranking.csv',
                    nargs='+',default='./results/*.ranking.csv')
parser.add_argument('--ground_truth_dir','-gt',help='Directory where the ground truth file are stored. If none is specified, defaults to ./ground_truth', default='./ground_truth')
parser.add_argument('--ground_truth_spec','-gts',help='File associating ground truth files and input data')
parser.add_argument('--ground_truth_type','-t',help='Type of entity to extract from ground truth', default='')
parser.add_argument('--ground_truth_idcolname','-i',help='Name of the column containing id information in the ground truth. Default: gt', default='gt')
parser.add_argument('--ground_truth_typecolname','-y',help='Name of the column containing type of entity information in the ground truth. ', default='')
parser.add_argument('--output_dir','-o',help='Output directory',default='./results')
parser.add_argument('--data_prop','-dp',help='JSON specification files matching the input files with their properties (e.g number of objects)',nargs='+',default='./csv/csv_properties.json')
parser.add_argument('--first_colname','-c',help='Name of the first column in the input files. Default: Objects',default='Objects')



def computeMetrics(input_file,ground_truth,data_properties='./csv/csv_properties.json',output_dir='./results',first_col_name='Objects',gt_idcolname='gt',gt_typecolname='',gt_type=''):
	rankings = metrics.getRankings(input_file)
	header = rankings.columns.values
	ranking_names = numpy.extract(header!=first_col_name,header) #filter out name of first column (only keep names of columns that correspond to rankings)
	num_gt = metrics.getNumberRelevantEntities(ground_truth,idcolname=gt_idcolname,typecolname=gt_typecolname,typegt=gt_type)
	with(open(data_properties,'r')) as f:
		num_objects = int(json.load(f)['datasets'][os.path.splitext(re.sub('avf.|fca.|support\d+\.|conf\d+\.|ranking.','',os.path.basename(input_file)))[0].replace('.ranking','')+'.csv']['objects'])
	metrics_dict={}
	for n in ranking_names:
		print(n)
		ranks = numpy.sort(rankings[n].values.compute())
		ndcg_n = metrics.ndcg(ranks,num_gt)
		rl_n = metrics.ranking_loss(ranks,num_objects)
		metrics_dict[n] = {'ndcg':ndcg_n,'ranking_loss':rl_n}
	#set output file paths
	outputfile = os.path.join(output_dir,os.path.splitext(os.path.basename(input_file.replace('.ranking','')))[0]+'.metrics.csv')
	#prepare content to write to file
	sorted_ranking_names = sorted(ranking_names)
	file_header = 'Measure,'+','.join(sorted_ranking_names)+'\n'
	measures = set(itertools.chain(*(tuple(metrics_dict[r].keys()) for r in ranking_names)))
	file_content = '\n'.join([m+','+','.join(str(metrics_dict[r][m]) for r in sorted_ranking_names) for m in measures])
	#write the results file
	with(open(outputfile,'w')) as f:
		f.write(file_header+file_content)
	
if __name__ == '__main__':
	#retrieving the arguments from the command line
	args = parser.parse_args()
	glob_path = args.glob_inputpath
	glob_path = ([glob_path] if isinstance(glob_path,str) else glob_path)
	inputfiles = [f for f in parsers.multiple_globs(*glob_path)]
	print(inputfiles)
	gt_dir = args.ground_truth_dir
	data_prop = args.data_prop
	colname = args.first_colname
	if len(inputfiles) == 0:
		sys.exit('No files to process. Stopping run')
	else:
		outputdir = args.output_dir
		if not args.ground_truth_spec:
			gt_files=[os.path.join(gt_dir,os.path.splitext(re.sub('avf.|fca.|support\d+\.|conf\d+\.|ranking.','',os.path.basename(i)))[0]+'_gt.csv') for i in inputfiles]
		else:
			gt_spec = args.ground_truth_spec
			gt_files = parsers.getGtFilesFromSpec(inputfiles,gt_spec)
			idname = args.ground_truth_idcolname
			typename = args.ground_truth_typecolname
			typeentity = args.ground_truth_type
		#processing input files
		Parallel(n_jobs=-1, verbose=1, backend="threading")(
	                                                       delayed(computeMetrics)
	                                                       (input_file=inputfiles[i],ground_truth=[gt_files[i]],
	                                                        data_properties=data_prop, output_dir=outputdir,first_col_name=colname,gt_idcolname=idname,gt_typecolname=typename,gt_type=typeentity)
	                                                        for i in range(len(inputfiles)))

